<?php

/**
 * @file
 * Hooks provided by the Emailoctopus module.
 */

use Drupal\webform\Plugin\WebformHandlerInterface;
use Drupal\webform\WebformSubmissionInterface;


/**
 * hook_emailoctopus_contact_data_alter
 *
 * Alters the contact data that is sent to Emailoctopus.
 * This hook is triggered on both create and update of contacts.
 *
 * @param array $data - The data received from a constant contact form.
 * @param object $body - The content of the request that is sent to the Emailoctopus API
 * @return void
 */
function hook_emailoctopus_contact_data_alter(array $data, object &$body) {
  // Add custom field values
  // Set a value directly.
  $body->custom_fields[] = (object) [
    'custom_field_id' => '00000000-0000-0000-0000-0000000000',  // The UUID of the custom field
    'value' => 'Some Value', // A value.
  ];

  // Add custom field values
  // Set based on $data value.
  $body->custom_fields[] = (object) [
    'custom_field_id' => '00000000-0000-0000-0000-0000000000',  // The UUID of the custom field
  ];

}

/**
 * emailoctopus_contact_create_data_alter
 *
 * Alters the contact data that is sent to Emailoctopus.
 * This hook is triggered only on creation of a contact.
 *
 * @param array $data - The data received from a constant contact form.
 * @param object $body - The content of the request that is sent to the Emailoctopus API
 * @return void
 */
function hook_emailoctopus_contact_create_data_alter(array $data, object &$body) {
}


/**
 * emailoctopus_contact_update_data_alter
 *
 * Alters the contact data that is sent to Emailoctopus.
 * This hook is triggered only on update of a contact.
 *
 * @param array $data - The data received from a constant contact form.
 * @param object $body - The content of the request that is sent to the Emailoctopus API
 * @return void
 */
function hook_emailoctopus_contact_update_data_alter(array $data, object &$body) {
}

/**
 * Alter mergevars before they are sent to Emailoctopus.
 *
 * @param array $mergevars
 *   The current mergevars.
 * @param WebformSubmissionInterface $submission
 *   The webform submission entity used to populate the mergevars.
 * @param WebformHandlerInterface $handler
 *   The webform submission handler used to populate the mergevars.
 *
 * @ingroup webform_emailoctopus
 */
function hook_emailoctopus_lists_mergevars_alter(&$mergevars, WebformSubmissionInterface $submission, WebformHandlerInterface $handler) {

}
