<?php

namespace Drupal\emailoctopus\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\emailoctopus\Service\Emailoctopus;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
/**
 * Emailoctopus Callback Controller.
 *
 * @package Drupal\emailoctopus\Controller
 */
class AuthenticationCallback extends ControllerBase {
  /**
   * Config factory service.
   *
   * @var Drupal\Core\Config\ConfigFactory
   */
  protected $config;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * GuzzleHttp\Client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $client;

  /**
   * Constructor function.
   *
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   Config factory.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Messenger interface.
   * @param Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request Stack.
   * @param GuzzleHttp\Client $client
   *   Guzzle HTTP client.
   * @param \Drupal\emailoctopus\Service\Emailoctopus $emailOctopus
   *   Constant contact service.
   */
  public function __construct(ConfigFactory $config, MessengerInterface $messenger, RequestStack $request_stack, Client $client, Emailoctopus $emailOctopus) {
    $this->config = $config;
    $this->messenger = $messenger;
    $this->client = $client;
    $this->requestStack = $request_stack;
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('http_client'),
      $container->get('emailoctopus')
    );
  }

}
