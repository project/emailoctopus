<?php

namespace Drupal\emailoctopus\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\emailoctopus\Service\Emailoctopus;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines CustomFieldsController class.
 */
class CustomFieldsController extends ControllerBase {

  /**
   * Constructor function.
   *
   * @param \Drupal\emailoctopus\Service\Emailoctopus $emailOctopus
   *   Constant contact service.
   */
  public function __construct(Emailoctopus $emailOctopus) {
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('emailoctopus')
    );
  }

  /**
   * Display the markup.
   *
   * @return array
   *   Return markup array.
   */
  public function content() {
    $fields = array();

    $lists = $this->emailOctopus->getMailingLists();

    if ($lists && count($lists) > 0) {
      $fields = $this->emailOctopus->getCustomFields(false);
    }

    $header = ['Custom Field Name', 'Field Type', 'Custom Field ID'];
    $rows = [];

    if ($fields && count($fields->custom_fields) > 0)  {
      $markup = $this->t('Custom fields available for your account:') . '<ul>';
      foreach ($fields->custom_fields as $field) {
        $rows[] = [
          $this->t($field->label),
          $this->t($field->type),
          [
            'data' => [
              '#markup' => '<code>' . $field->custom_field_id . '</code>'
            ]
          ]
        ];
      }

      
    }

    return [
      '#type' => 'table',
      '#header' => $header,
      '#rows' => $rows,
      '#empty' => $this->t('There are no custom fields found.')
    ];
  }

}