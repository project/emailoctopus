<?php

namespace Drupal\emailoctopus\Plugin;

use Drupal\emailoctopus\Plugin\MailingListField;

/**
 * Class MailingListFieldSerializer
 *
 * @package Ideneal\EmailOctopus\Serializer
 */
class MailingListFieldSerializer extends ApiSerializer
{
    /**
     * @param MailingListField $object
     *
     * @return array
     */
    public static function serialize($object): array
    {
        return [
            'label'    => $object->getLabel(),
            'tag'      => $object->getTag(),
            'type'     => $object->getType(),
            'fallback' => $object->getFallback(),
        ];
    }

    /**
     * @param array $json
     *
     * @return MailingListField
     */
    public static function deserializeSingle(array $json)
    {
        $field = new MailingListField();
        $field
        ->setLabel($json['label'])
        ->setTag($json['tag'])
        ->setType($json['type'])
        ->setFallback($json['fallback'])
        ;

        return $field;
    }
}