<?php

namespace Drupal\emailoctopus\Plugin\Field\FieldFormatter;

use Drupal\options\Plugin\Field\FieldFormatter\OptionsDefaultFormatter;

/**
 * Plugin implementation of the 'emailoctopus_lists_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "emailoctopus_lists_formatter",
 *   label = @Translation("Default"),
 *   field_types = {
 *     "emailoctopus_lists",
 *   }
 * )
 */
class EmailoctopusListDefaultFormatter extends OptionsDefaultFormatter {
}