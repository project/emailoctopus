<?php

namespace Drupal\emailoctopus\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsButtonsWidget;

/**
 * Plugin implementation of the 'emailoctopus_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "emailoctopus_lists_checkbox",
 *   label = @Translation("Check boxes/radio buttons"),
 *   field_types = {
 *     "emailoctopus_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class EmailoctopusListCheckboxesWidget extends OptionsButtonsWidget {
}