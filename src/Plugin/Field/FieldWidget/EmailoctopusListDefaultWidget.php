<?php

namespace Drupal\emailoctopus\Plugin\Field\FieldWidget;

use Drupal\Core\Field\Plugin\Field\FieldWidget\OptionsSelectWidget;

/**
 * Plugin implementation of the 'emailoctopus_lists_default' widget.
 *
 * @FieldWidget(
 *   id = "emailoctopus_lists_default",
 *   label = @Translation("Select list"),
 *   field_types = {
 *     "emailoctopus_lists"
 *   },
 *   multiple_values = TRUE
 * )
 */
class EmailoctopusListDefaultWidget extends OptionsSelectWidget {
}