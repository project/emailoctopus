<?php

namespace Drupal\emailoctopus\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\emailoctopus\Service\Emailoctopus;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides block plugin definitions for IK Emailoctopus blocks.
 *
 * @see \Drupal\emailoctopus\Plugin\Block\EmailoctopusBlock
 */
class EmailoctopusBlockDerivative extends DeriverBase implements ContainerDeriverInterface {

  /**
   * Drupal\Core\Config\ConfigFactoryInterface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\emailoctopus\Service\Emailoctopus.
   *
   * @var \Drupal\emailoctopus\Service\Emailoctopus
   *   Constant contact service.
   */
  protected $emailOctopus;

  /**
   * EmailoctopusBlockDerivative constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   ConfigFactoryInterface.
   * @param \Drupal\emailoctopus\Service\Emailoctopus $emailOctopus
   *   Constant contact service.
   */
  public function __construct(ConfigFactoryInterface $configFactory, Emailoctopus $emailOctopus) {
    $this->configFactory = $configFactory;
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('config.factory'),
      $container->get('emailoctopus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $lists = $this->emailOctopus->getEnabledMailingLists(false);

    if ($lists) {
      foreach ($lists as $id => $value) {
        if ($value->enabled === true) {
          $this->derivatives[$id] = $base_plugin_definition;
          $this->derivatives[$id]['admin_label'] = $lists[$id]->name . '  Signup Block';
        }
      }
    }

    return $this->derivatives;
  }

}
