<?php

namespace Drupal\emailoctopus\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\emailoctopus\Service\Emailoctopus;

/**
 * Provides a constant contact signup form per list that is enabled.
 *
 * @Block(
 *   id = "emailoctopus",
 *   admin_label = @Translation("Emailoctopus Signup Form"),
 *   deriver = "Drupal\emailoctopus\Plugin\Derivative\EmailoctopusBlockDerivative"
 * )
 */
class EmailoctopusBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Form\FormBuilderInterface.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * Drupal\emailoctopus\Service\Emailoctopus.
   *
   * @var \Drupal\emailoctopus\Service\Emailoctopus
   *   Constant contact service.
   */
  protected $emailOctopus;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, Emailoctopus $emailOctopus) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $container->get('emailoctopus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $ccConfig = $this->emailOctopus->getConfig();
    $customFields = $this->emailOctopus->getCustomFields();

    // Custom Fields
    if ($customFields && $customFields->custom_fields && count($customFields->custom_fields) > 0) {
      $form['custom_fields'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Custom Fields'),
        '#description' => $this->t('Select any <a href="https://knowledgebase.emailoctopus.com/articles/KnowledgeBase/33120-Create-and-Manage-Custom-Contact-Fields?lang=en_US" target="_blank" rel="nofollow noreferrer">custom fields from your Emailoctopus account</a> to add to the signup form.'),
        '#tree' => true
      ];

      foreach ($customFields->custom_fields as $field) {
        $form['custom_fields'][$field->custom_field_id] = [
          '#type' => 'fieldset',
          '#title' => $this->t($field->label),
        ];
        $form['custom_fields'][$field->custom_field_id]['display'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Display ' . $field->label . ' field'),
          '#default_value' => isset($config['custom_fields'][$field->custom_field_id]['display']) ? $config['custom_fields'][$field->custom_field_id]['display'] : NULL,
        ];
        $form['custom_fields'][$field->custom_field_id]['required'] = [
          '#type' => 'checkbox',
          '#title' => $this->t('Require ' . $field->label . ' field'),
          '#default_value' => isset($config['custom_fields'][$field->custom_field_id]['required']) ? $config['custom_fields'][$field->custom_field_id]['required'] : NULL,
        ];
        $form['custom_fields'][$field->custom_field_id]['name'] = [
          '#type' => 'hidden',
          '#value' => $field->name,
        ];
        $form['custom_fields'][$field->custom_field_id]['type'] = [
          '#type' => 'hidden',
          '#value' => $field->type,
        ];
        $form['custom_fields'][$field->custom_field_id]['label'] = [
          '#type' => 'hidden',
          '#value' => $field->label,
        ];

        if ($field->type === 'date') {
          $form['custom_fields'][$field->custom_field_id]['#description'] = $this->t('Requires the <a href="https://www.drupal.org/docs/8/core/modules/datetime" target="_blank" rel="nofollow noreferrer">datetime</a> module to be installed.');
        }
      }
    }

    $form['body'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Body'),
      '#default_value' => isset($config['body']) ? $config['body']['value'] : NULL,
      '#format' => isset($config['format']) ? $config['body']['format'] : NULL,
    ];
    $form['success_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Custom Success Message'),
      '#default_value' => isset($config['success_message']) ? $config['success_message'] : NULL,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    $values = $form_state->getValues();
    $ccConfig = $this->emailOctopus->getConfig();

    $this->configuration['custom_fields'] = $values['custom_fields'];
    $this->configuration['body'] = $values['body'];
    $this->configuration['success_message'] = $values['success_message'];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $listConfig = $this->getConfiguration();
    $listConfig['list_id'] = str_replace('emailoctopus:', '', $this->getPluginId());

    return $this->formBuilder->getForm('Drupal\emailoctopus\Form\EmailoctopusBlockForm', $listConfig);
  }

}
