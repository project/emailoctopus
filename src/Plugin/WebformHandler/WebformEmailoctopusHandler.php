<?php

namespace Drupal\emailoctopus\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformTokenManagerInterface;
use Drupal\emailoctopus\Service\Emailoctopus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Form submission to Emailoctopus handler.
 *
 * @WebformHandler(
 *   id = "emailoctopus",
 *   label = @Translation("Emailoctopus"),
 *   category = @Translation("Emailoctopus"),
 *   description = @Translation("Sends a form submission to a Emailoctopus list."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 * )
 */
class WebformEmailoctopusHandler extends WebformHandlerBase {

    /**
   * Drupal\Core\Cache\CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   *   Drupal cache.
   */
    protected $cache;

    /**
   * Drupal\emailoctopus\Service\Emailoctopus.
   *
   * @var \Drupal\emailoctopus\Service\Emailoctopus
   *   Constant contact service.
   */
    protected $emailOctopus;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTokenManagerInterface
   */
  protected $token_manager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $instance->setCacheManager($container->get('cache.default'));
    $instance->setTokenManager($container->get('webform.token_manager'));
    $instance->setEmailoctopus($container->get('emailoctopus'));
    return $instance;
  }

  /**
   * Set Cache dependency
   */
  protected function setCacheManager(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }
  /**
   * Set Token Manager dependency
   */
  protected function setTokenManager(WebformTokenManagerInterface $token_manager) {
    $this->tokenManager = $token_manager;
  }

  /**
   * Set Emailoctopus dependency
   */
  protected function setEmailoctopus(Emailoctopus $emailOctopus) {
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $lists = $this->emailOctopus->getMailingLists();

    $email_summary = $this->configuration['email'];
    if (!empty($fields[$this->configuration['email']])) {
      $email_summary = $fields[$this->configuration['email']]['#title'];
    }
    $email_summary = '<strong>' . $this->t('Email') . ': </strong>' . $email_summary;


    $list_summary = $this->configuration['list'];
    if (!empty($lists[$this->configuration['list']])) {
      $list_summary = $lists[$this->configuration['list']]->name;
    }
    $list_summary = '<strong>' . $this->t('List') . ': </strong>' . $list_summary;

    $markup = "$email_summary<br/>$list_summary";
    return [
      '#markup' => $markup,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list' => '',
      'email' => '',
      'mergevars' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $enabled = $this->configFactory->get('emailoctopus.enabled_lists')->getRawData();
    $lists = $this->emailOctopus->getMailingLists();

    $options = [];
    foreach ($lists as $list) {
      if ($enabled[$list->getId()] == 1) {
        $options[$list->getId()] = $list->name;
      }
    }

    $form['emailoctopus'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Emailoctopus settings'),
      '#attributes' => ['id' => 'webform-emailoctopus-handler-settings'],
    ];

    $form['emailoctopus']['update'] = [
      '#type' => 'submit',
      '#value' => $this->t('Refresh lists & groups'),
      '#ajax' => [
        'callback' => [$this, 'ajaxEmailoctopusListHandler'],
        'wrapper' => 'webform-emailoctopus-handler-settings',
      ],
      '#submit' => [[get_class($this), 'emailoctopusUpdateConfigSubmit']],
    ];

    $form['emailoctopus']['list'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('List'),
      '#required' => TRUE,
      '#empty_option' => $this->t('- Select an option -'),
      '#default_value' => $this->configuration['list'],
      '#options' => $options,
      '#ajax' => [
        'callback' => [$this, 'ajaxEmailoctopusListHandler'],
        'wrapper' => 'webform-emailoctopus-handler-settings',
      ],
      '#description' => $this->t('Select the list you want to send this submission to. Alternatively, you can also use the Other field for token replacement.'),
    ];

    $fields = $this->getWebform()->getElementsInitializedAndFlattened();
    $options = [];
    foreach ($fields as $field_name => $field) {
      if (in_array($field['#type'], ['email', 'webform_email_confirm'])) {
        $options[$field_name] = $field['#title'];
      }
    }

    $default_value = $this->configuration['email'];
    if (empty($this->configuration['email']) && count($options) == 1) {
      $default_value = key($options);
    }
    $form['emailoctopus']['email'] = [
      '#type' => 'webform_select_other',
      '#title' => $this->t('Email field'),
      '#required' => TRUE,
      '#default_value' => $default_value,
      '#options' => $options,
      '#empty_option'=> $this->t('- Select an option -'),
      '#description' => $this->t('Select the email element you want to use for subscribing to the Emailoctopus list specified above. Alternatively, you can also use the Other field for token replacement.'),
    ];

    $form['emailoctopus']['mergevars'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Merge vars'),
      '#default_value' => $this->configuration['mergevars'],
      '#description' => $this->t('You can map additional fields from your webform to fields in your Emailoctopus list, one per line. An example might be first_name: [webform_submission:values:first_name]. The result is sent as an array. You may use tokens.'),
    ];

    $form['emailoctopus']['token_tree_link'] = $this->tokenManager->buildTreeLink();

    return $form;
  }

  /**
   * Ajax callback to update Webform Emailoctopus settings.
   */
  public static function ajaxEmailoctopusListHandler(array $form, FormStateInterface $form_state) {
    return $form['settings']['emailoctopus'];
  }


  /**
   * Submit callback for the refresh button.
   */
  public static function emailoctopusUpdateConfigSubmit(array $form, FormStateInterface $form_state) {
    // Trigger list and group category refetch by deleting lists cache.
    $cache = \Drupal::cache();
    $cache->delete('emailoctopus.lists');
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values['emailoctopus'][$name])) {
        $this->configuration[$name] = $values['emailoctopus'][$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // If update, do nothing
    if ($update) {
      return;
    }

    $fields = $webform_submission->toArray(TRUE);

    $configuration = $this->tokenManager->replace($this->configuration, $webform_submission);

    // Check if we are using a token for the list field & replace value.
    if (isset($this->configuration['list']) && strpos($this->configuration['list'], '[webform_submission:values:') !== false) {
      $configuration['list'] = null;
      $fieldToken = str_replace(['[webform_submission:values:', ']'], '', $this->configuration['list']);

      if (isset($fields['data'][$fieldToken])) {
        if (is_string($fields['data'][$fieldToken])) {
          $configuration['list'] = [$fields['data'][$fieldToken]];
        }
        else if (is_array($fields['data'][$fieldToken])) {
          $configuration['list'] = $fields['data'][$fieldToken];
        }
      }
    }

    // Email could be a webform element or a string/token.
    if (!empty($fields['data'][$configuration['email']])) {
      $email = $fields['data'][$configuration['email']];
    }
    else {
      $email = $configuration['email'];
    }

    $mergevars = Yaml::decode($configuration['mergevars']) ?? [];

    // Allow other modules to alter the merge vars.
    // @see hook_emailoctopus_lists_mergevars_alter().
    $entity_type = 'webform_submission';
    \Drupal::moduleHandler()->alter('emailoctopus_lists_mergevars', $mergevars, $webform_submission, $entity_type);
    \Drupal::moduleHandler()->alter('webform_emailoctopus_lists_mergevars', $mergevars, $webform_submission, $this);

    $handler_link = Link::createFromRoute(
      t('Edit handler'),
      'entity.webform.handler.edit_form',
      [
        'webform' => $this->getWebform()->id(),
        'webform_handler' => $this->getHandlerId(),
      ]
    )->toString();

    $submission_link = ($webform_submission->id()) ? $webform_submission->toLink($this->t('View'))->toString() : NULL;

    $context = [
      'link' => $submission_link . ' / ' . $handler_link,
      'webform_submission' => $webform_submission,
      'handler_id' => $this->getHandlerId(),
    ];

    if (!empty($configuration['list']) && !empty($email)) {
      $data = array_merge(['email_address' => $email], $mergevars);
      $this->emailOctopus->submitContactForm($data , $configuration['list']);
    }
    else {
      if (empty($configuration['list'])) {
        \Drupal::logger('webform_submission')->warning(
          'No Emailoctopus list was provided to the handler.',
          $context
        );
      }
      if (empty($email)) {
        \Drupal::logger('webform_submission')->warning(
          'No email address was provided to the handler.',
          $context
        );
      }
    }
  }

}
