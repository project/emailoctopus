<?php

namespace Drupal\emailoctopus\Plugin;


use Psr\Http\Message\ResponseInterface;
use Drupal\emailoctopus\ApiSerializerInterface;

/**
 * Class ApiSerializer
 *
 * @package Ideneal\EmailOctopus\Serializer
 */
abstract class ApiSerializer extends JsonDeserializer implements ApiSerializerInterface
{
    /**
     * @param ResponseInterface $response
     *
     * @return array|object
     */
    public static function deserialize(ResponseInterface $response)
    {
        $json = json_decode($response->getBody(), true);
        return isset($json['data']) && isset($json['paging']) ? static::deserializeMultiple($json['data']) : static::deserializeSingle($json);
    }
}