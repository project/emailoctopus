<?php

namespace Drupal\emailoctopus\Service;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Session\AccountProxy;
use Drupal\Core\Site\Settings;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\ClientException;
use Drupal\emailoctopus\Plugin\EmailOctopusPlugin;
use Drupal\emailoctopus\Plugin\Contact;
use Drupal\Core\Url;

/**
 * Class Emailoctopus.
 *
 * Class to handle API calls to Emailoctopus.
 */
class Emailoctopus {

  use StringTranslationTrait;

  /**
   * Drupal\Core\Cache\CacheBackendInterface.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   *   Drupal cache.
   */
  protected $cache;

  /**
   * Drupal\Core\Config\ConfigFactory.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   *   Drupal config.
   */
  protected $config;

  /**
   * Drupal\Core\Session\AccountProxy.
   *
   * @var \Drupal\Core\Session\AccountProxy
   *   Drupal current user.
   */
  protected $currentUser;

  /**
   * Drupal\Core\Logger\LoggerChannelFactoryInterface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   *   Drupal logging.
   */
  protected $loggerFactory;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\Core\Site\Settings.
   *
   * @var \Drupal\Core\Site\Settings
   *   Drupal site settings.
   */
  protected $settings;

  /**
   * GuzzleHttp\Client.
   *
   * @var \GuzzleHttp\Client
   *   Guzzle HTTP Client.
   */
  protected $httpClient;

  /**
   * \Drupal\Core\Extension\ModuleHandlerInterface
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   *   Module handler interface
   */
  protected $moduleHandler;

  /**
   * Constructs the class.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   The interface for cache implementations.
   * @param \Drupal\Core\Config\ConfigFactory $config
   *   The configuration object factory.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The factory for logging channels.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The runtime messages sent out to individual users on the page.
   * @param \Drupal\Core\Site\Settings $settings
   *   The read settings that are initialized with the class.
   * @param \GuzzleHttp\Client $httpClient
   *   The client for sending HTTP requests.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   */
  public function __construct(CacheBackendInterface $cache, ConfigFactory $config, AccountProxy $currentUser, LoggerChannelFactoryInterface $loggerFactory, MessengerInterface $messenger, Settings $settings, Client $httpClient, ModuleHandlerInterface $moduleHandler) {
    $this->cache = $cache;
    $this->config = $config;
    $this->currentUser = $currentUser;
    $this->loggerFactory = $loggerFactory->get('emailoctopus');
    $this->messenger = $messenger;
    $this->settings = $settings;
    $this->httpClient = $httpClient;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Returns the configurations for the class.
   *
   * @return array
   *   Returns an array with all configuration settings.
   */
  public function getConfig() {
    // Get our settings from settings.php.
    $settings = $this->settings::get('emailoctopus');

    $api_key = isset($settings['api_key']) ? $settings['api_key'] : NULL;
    $configType = 'settings.php';

    // If nothing is in settings.php, let's check our config files.
    if (!$settings) {
      $api_key = $this->config->get('emailoctopus.config')->get('api_key');
      $configType = 'config';
    }

    $code_verifier = $this->config->get('emailoctopus.pkce')->get('code_verifier');

    if (!$this->moduleHandler->moduleExists('automated_cron') && !$this->moduleHandler->moduleExists('ultimate_cron') && (int)$this->currentUser->id() !== 0 && $this->currentUser->hasPermission('administer constant contact configuration')) {
      $this->messenger->addMessage($this->t('It is recommended to install automated_cron or make sure that cron is run regularly to refresh access tokens from Emailoctopus API.'), 'warning');
    }

    return [
      'api_key' => $api_key,
      'config_type' => $configType, // Application api_key and other info found in settings.php or via config
      'code_verifier' => $code_verifier,
    ];
  }

  public function base64UrlEncode($string) {
    $base64 = base64_encode($string);
    $base64 = trim($base64, '=');
    $base64url = strtr($base64, '+/', '-_');

    return $base64url;
  }

  /**
   * Generates a random PKCE Code Verifier.
   *
   * https://datatracker.ietf.org/doc/html/rfc7636#section-4.1
   * https://v3.developer.emailoctopus.com/api_guide/pkce_flow.html#generate-the-code-verifier
   *
   * @return string
   */
  public function generateCodeVerifier() {
    $random = bin2hex(openssl_random_pseudo_bytes(32));
    $verifier = $this->base64UrlEncode(pack('H*', $random));

    return $verifier;
  }

  /**
   * Return the encoded code challenge for the PKCE Code Verifier to send to API Authorization Endpoint.
   *
   * https://v3.developer.emailoctopus.com/api_guide/pkce_flow.html#generate-the-code-challenge
   *
   * @param string $codeVerifier
   * @return string
   */
  public function getCodeChallenge($codeVerifier) {
    return $this->base64UrlEncode(pack('H*', hash('sha256', $codeVerifier)));
  }

  /**
   * Shared method to generate the rest of the request body.
   *
   * @NOTE that email_address, permission_to_send are not added hear since the fields are
   * different per api call type. For example, the list_memberships, the email_address field.
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/contacts_create_or_update.html#method-request-body
   *
   * @param array $data - posted data from our form
   * @param object $body - An object already generated.
   * @return object $body
   */
  protected function buildResponseBody(array $data, object $body) {
    if (isset($data['custom_fields']) && count($data['custom_fields']) > 0) {
      foreach ($data['custom_fields'] as $id => $value) {
        $body->custom_fields[] = ['custom_field_id' => $id, 'value' => $value];
      }
    }

    return $body;
  }

  /**
   * Creates a new contact by posting to Emailoctopus API.
   *
   * @param array $data
   *   Array of data to send to Emailoctopus.
   *    Requires 'email_address' key.
   *    Can also accept 'first_name' and 'last_name'.
   * @param array $listIDs
   *   An array of list UUIDs where we want to add this contact.
   *
   * @see https://v3.developer.emailoctopus.com/api_reference/index.html#!/Contacts/createContact
   */
  private function createContact(array $data, $listIDs) {
    $config = $this->getConfig();

    $body = (object) [
      'email_address' => (object) [
        'address' => NULL,
        'permission_to_send' => NULL,
      ],
      'first_name' => NULL,
      'last_name' => NULL,
      'create_source' => NULL,
      'list_memberships' => NULL,
    ];

    $body = $this->buildResponseBody($data, $body);

    // Add our required fields.
    $body->email_address->address = $data['email_address'];
    $body->email_address->permission_to_send = 'implicit';
    $body->list_memberships = $listIDs;
    $body->create_source = 'Account';

    $this->moduleHandler->invokeAll('emailoctopus_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('emailoctopus_contact_create_data_alter', [$data, &$body]);

    try {
      $response = $this->httpClient->request('POST', $config['contact_url'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
        'body' => json_encode($body),
      ]);

      $this->handleResponse($response, 'createContact');
    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e->getMessage());

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }


  /**
   * Fetch the details of a single campaign.
   *
   * @param string $id
   *   The id of the campaign.
   *
   * @return mixed
   *   An stdClass of the campaign.
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/email_campaign_id.html
   */
  public function getCampaign(string $id) {
    $config = $this->getConfig();
    try {
      $response = $this->httpClient->request('GET', $config['campaigns_url'] . '/' . $id, [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      return $json;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e->getMessage());
    }
  }

  /**
   * Get the campaign activity details by id.
   *
   * @param string $id
   *   The id of the activity.
   *
   * @return mixed
   *   A stdClass of the activity.
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/email_campaigns_activity_id.html
   */
  public function getCampaignActivity(string $id) {
    $config = $this->getConfig();
    $url = $config['campaign_activity_url'] . '/' . $id;
    $url .= '?include=permalink_url';
    try {
      $response = $this->httpClient->request('GET', $url, [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      return $json;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e->getMessage());
    }
  }

  /**
   * Returns a list of the campaigns.
   *
   * @param array $status
   *   An option to filter campaigns by status.
   *
   * @return array
   *   An array of campaigns.
   * @throws \GuzzleHttp\Exception\GuzzleException
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/email_campaigns_collection.html
   */
  public function getCampaigns($status = []) {
    $config = $this->getConfig();
    try {
      $response = $this->httpClient->request('GET', $config['campaigns_url'], [
        'headers' => [
          'Authorization' => 'Bearer ' . $config['api_key'],
          'cache-control' => 'no-cache',
          'content-type' => 'application/json',
          'accept' => 'application/json',
        ],
      ]);

      $this->updateTokenExpiration();
      $json = json_decode($response->getBody()->getContents());
      $list = [];

      foreach ($json->campaigns as $campaign) {
        if (!empty($status) && in_array($campaign->current_status, $status)) {
          $list[] = $campaign;
        }
        else if (empty($status)) {
          $list[] = $campaign;
        }

      }
      return $list;
    }
    catch (RequestException $e) {
      $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e->getMessage());
    }
  }

  /**
   * Checks if a contact exists already.
   *
   * @param array $data
   *   Array of data to send. 'email_address' key is required.
   *
   * @return array
   *   Returns a json response that determines if a contact
   *   already exists or was deleted from the list.
   *
   * @see https://v3.developer.emailoctopus.com/api_reference/index.html#!/Contacts/getContact
   */
  private function getContact(array $data) {
    $config = $this->getConfig();

    try {

      $exists = array();

      $api_key = $config['api_key'];
      if(isset($config['api_key']) && $config['api_key']) {

        $exists['contacts'][0] = true;

      }

      return $exists;

    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e->getMessage());

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }

  /**
   * Gets contact lists from Emailoctopus API.
   *
   * @param $cached
   *  Whether to return a cached response or not.
   *  @see https://www.drupal.org/project/emailoctopus/issues/3282088 and https://v3.developer.emailoctopus.com/api_guide/faqs_manage_applications.html
   *  Cron run perhaps was calling Drupal cached version which may have allowed refresh tokens to expire.
   *
   * @return array
   *   Returns an array of lists that the account has access to.
   *
   * @see https://v3.developer.emailoctopus.com/api_reference/index.html#!/Contact_Lists/getLists
   *
   */
  public function getMailingLists($cached = true) {
    $config = $this->getConfig();

    $cid = 'emailoctopus.lists';
    $cache = ($cached === true ? $this->cache->get($cid) : null);

    if ($cache && $cache->data && count($cache->data) > 0) {
      return $cache->data;
    }
    else {
      // Update access tokens.
      $this->refreshToken(false);

      try {

        $api_key = $config['api_key'];
        if(isset($config['api_key']) && $config['api_key']) {

          $convertkitAPI = new EmailOctopusPlugin($api_key);

          $mailing_lists = $convertkitAPI->getMailingLists();

          $this->updateTokenExpiration();

          if (!empty($mailing_lists)) {
            foreach ($mailing_lists as $list) {
              $lists[$list->getId()] = $list;
            }

            $this->saveContactLists($lists);
            return $lists;
          }
          else {
            $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
          }
        }
      }
      catch (RequestException $e) {
        $this->handleRequestException($e);
        $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
      }
      catch (ClientException $e) {
        $this->handleRequestException($e);
        $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
      }
      catch (\Exception $e) {
        $this->loggerFactory->error($e->getMessage());
        $this->messenger->addMessage($this->t('There was a problem getting your available contact lists.'), 'error');
      }

    }
  }

  /**
   * Returns custom fields available
   *
   * @param $cached
   *  Whether to return a cached response or not.
   *
   * @return mixed
   *   A stdClass of custom fields.
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/get_custom_fields.html
   */
  public function getCustomFields($cached = true) {
    $config = $this->getConfig();
    $cid = 'emailoctopus.custom_fields';
    $cache = ($cached === true ? $this->cache->get($cid) : null);

    if ($cache && !is_null($cache) && $cache->data && property_exists($cache->data, 'custom_fields')) {
      return $cache->data;
    }
    else {

      $api_key = $config['api_key'];

      $custom_fields_data = new \stdClass();

      $custom_fields = array();

      if(isset($config['api_key']) && $config['api_key']) {

       $convertkitAPI = new EmailOctopusPlugin($api_key);

       $mailing_lists = $convertkitAPI->getMailingLists();

       foreach ($mailing_lists as $key => $mailing_list) {
        $fields = $mailing_list->getFields();

        foreach ($fields as $key => $field) {
          $tag = $field->getTag();
          if($tag != 'EmailAddress') {
            $label = $field->getLabel();
            $type = $field->getType();

            $data_value = new \stdClass();
            $data_value->name = $tag;
            $data_value->label = $label;
            $data_value->type = $type;
            $data_value->custom_field_id = $tag;

            if(!in_array($data_value, $custom_fields)) {
              $custom_fields[] = $data_value;
            }
          }
        }
      }

      $custom_fields_data->custom_fields = $custom_fields;

      try {

        $this->updateTokenExpiration();

        $this->cache->set($cid, $custom_fields_data);

        return $custom_fields_data;
      }
      catch (RequestException $e) {
        $this->handleRequestException($e);
      }
      catch (ClientException $e) {
        $this->handleRequestException($e);
      }
      catch (\Exception $e) {
        $this->loggerFactory->error($e->getMessage());
      }
    }
  }
}

  /**
   * Get Enabled Contact Lists
   *
   * @param boolean $cached
   * @return array $lists of enabled lists
   *
   * @see /Drupal/Form/EmailoctopusLists.php
   */
  public function getEnabledMailingLists($cached = true) {
    $lists = $this->getMailingLists($cached);

    return $lists;
  }

  /**
   * Get the permanent link of a campaign.
   *
   * @param string $id
   *   The campaign id.
   *
   * @return null|string
   *   The URL of the campaign's permanent link.
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPermaLinkFromCampaign(string $id) {
    $url = NULL;
    if (!$id) {
      return NULL;
    }
    $campaign = $this->getCampain($id);
    foreach ($campaign->campaign_activities as $activity) {
      if ($activity->role != 'permalink') {
        continue;
      }
      $act = $this->getCampaignActivity($activity->campaign_activity_id);
      if ($act) {
        return $act->permalink_url;
      }
    }
    return NULL;
  }

  /**
   * Handles an error
   *
   * @param [object] $error
   * @return [array] $return
   */
  protected function handleRequestException(object $e) {
    $response = $e->getResponse();
    $error = is_null($response) ? FALSE : json_decode($response->getBody());

    $message = 'RequestException: ';

    $errorInfo = [];

    if ($error && is_object($error)) {
      if (property_exists($error, 'error')) {
        $errorInfo[] = $error->error;
      }

      if (property_exists($error, 'message')) {
        $errorInfo[] = $error->message;
      }

      if (property_exists($error, 'error_description')) {
        $errorInfo[] = $error->error_description;
      }

      if (property_exists($error, 'errorCode')) {
        $errorInfo[] = $error->errorSummary;
      }

      if (property_exists($error, 'errorCode')) {
        $errorInfo[] = $error->errorCode;
      }
    }

    if(!empty($errorInfo)) {
      $message .= implode(', ', $errorInfo);

      $this->loggerFactory->error($message);

      // Return the error to show an error on form submission
      return ['error' => $message];
    }
  }

  /**
   * Handles API response for adding a contact.
   *
   * @param object $response
   *   The json_decoded json response.
   * @param string $method
   *   The name of the method that the response came from.
   *
   * @return array
   *   Returns an array that includes the method name and
   *   the statuscode except if it is coming from getContact method.
   *   Then it returns an array of the contact that matches.
   */
  private function handleResponse($response, $method) {
    if ((strtolower($response->getStatus()) === 'subscribed') || (strtolower($response->getStatus()) === 201)) {

      $this->loggerFactory->info('@method has been executed successfully.', ['@method' => $method]);

      $this->updateTokenExpiration();

      if ($method === 'getContact') {
        $json = json_decode($response->getBody()->getContents());

        return $json;
      }

      return [
        'method' => $method,
        'response' => $response->getStatus(),
      ];
    }
    else {
      $statuscode = $response->getStatus();
      $responsecode = $response->getReasonPhrase();

      $this->loggerFactory->error('Call to @method resulted in @status response. @responsecode', [
        '@method' => $method,
        '@status' => $statuscode,
        '@responsecode' => $responsecode,
      ]);

      return [
        'error' => 'There was a problem signing up. Please try again later.',
      ];
    }
  }

  /**
   * Submits a contact to the API.
   * Used to be used on CostantContactBlockForm but using $this->submitContactForm instead.
   * Determine if contact needs to be updated or created.
   *
   * @param array $data
   *   Data to create/update a contact.
   *   Requires a 'email_address' key.
   *   But can also accept 'first_name' and 'last_name' key.
   * @param array $listIDs
   *   An array of list UUIDs to post the contact to.
   *
   * @return array
   *   Returns an error if there is an error.
   *   Otherwise it sends the info to other methods.
   *
   * @see $this->updateContact
   * @see $this->putContact
   * @see $this->createContact
   */
  public function postContact(array $data = [], $listIDs = []) {
    $config = $this->getConfig();
    $enabled = $this->config->get('emailoctopus.enabled_lists')->getRawData();

    if (!$config['api_key'] || !$config['api_key'] || !$data) {
      $msg = 'Missing credentials for postContact';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    if (!$listIDs || count($listIDs) === 0) {
      $msg = 'A listID is required.';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    foreach ($listIDs as $listID) {
      if (!isset($enabled[$listID]) || $enabled[$listID] !== 1) {
        $msg = 'The listID provided does not exist or is not enabled.';

        $this->loggerFactory->error($msg);

        return [
          'error' => $msg,
        ];
      }
    }

    if (!isset($data['email_address'])) {
      $msg = 'An email address is required';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    // Refresh our tokens before every request.
    $this->refreshToken();

    // Check if contact already exists.
    $exists = (array) $this->getContact($data);

    // If yes, updateContact.
    // If no, createContact.
    // If previous deleted, putContact.
    if (isset($exists['contacts']) && count($exists['contacts']) > 0) {
      $this->updateContact($data, $exists['contacts'][0], $listIDs);
    }
    elseif ($exists && isset($exists['deleted_at'])) {
      $this->putContact($exists, $data, $listIDs);
    }
    else {
      $this->createContact($data, $listIDs);
    }
  }

  /**
   * Updates a contact if it already exists and has been deleted.
   *
   * @param array $data
   *   The $data provided originally. @see $this->postContact.
   * @param array $listIDs
   *   The list IDs we want to add contact to.
   *
   * @TODO perhaps combine this with updateContact. The difference is that $contact is
   * an array here and an object in updateContact.
   */
  private function putContact(array $contact, array $data, $listIDs) {
    $config = $this->getConfig();

    $body = (object) $contact;

    $body = $this->buildResponseBody($data, $body);

    $body->email_address->permission_to_send = 'implicit';
    // To resubscribe a contact after an unsubscribe update_source must equal Contact.
    // @see https://v3.developer.emailoctopus.com/api_guide/contacts_re-subscribe.html#re-subscribing-contacts
    $body->update_source = 'Contact';
    $body->list_memberships = $listIDs;

    $this->moduleHandler->invokeAll('emailoctopus_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('emailoctopus_contact_update_data_alter', [$data, &$body]);

    try {

      // put contact here

    }
    catch (RequestException $e) {
      // Return the error to show an error on form submission
      return $this->handleRequestException($e);
    }
    catch (ClientException $e) {
      return $this->handleRequestException($e);
    }
    catch (\Exception $e) {
      $this->loggerFactory->error($e->getMessage());

      // Return the error to show an error on form submission
      return ['error' => $e];
    }
  }

  /**
   * Makes authenticated request to Emailoctopus to refresh tokens.
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/server_flow.html#refreshing-an-access-token
   */
  public function refreshToken($updateLists = true) {
    $config = $this->getConfig();

    if (!$config['api_key']) {
      return FALSE;
    }

  }

  /**
   * Saves available contact lists to a cache.
   *
   * @param array $data
   *   An array of lists and list UUIDs from $this->getMailingLists.
   */
  public function saveContactLists(array $data) {
    $cid = 'emailoctopus.lists';
    $enabled = $this->config->get('emailoctopus.enabled_lists')->getRawData();

    // Add an enabled flag to the list data.
    foreach ($data as $key => $value) {
      $data[$key]->enabled = (isset($enabled[$key]) && $enabled[$key] === 1);
      $data[$key]->cached_on = strtotime('now');
    }

    $this->cache->set($cid, $data);
  }

  /**
   * Saves access and refresh tokens to our config database.
   *
   * @param object $data
   *   Data object of data to save the token.
   *
   * @see $this->refreshToken
   */
  private function saveTokens($data) {
    if ($data && property_exists($data, 'api_key')) {
      $tokens = $this->config->getEditable('emailoctopus.tokens');
      $tokens->clear('emailoctopus.tokens');
      $tokens->set('api_key', $data->api_key);
      $tokens->set('timestamp', strtotime('now'));

      if ($data->expires_in < $tokens->get('expires')) {
        $tokens->set('expires', $data->expires_in);
      }

      $tokens->save();

      $this->loggerFactory->info('New tokens saved at ' . date('Y-m-d h:ia', strtotime('now')));
    }
    else {
      $this->loggerFactory->error('There was an error saving tokens');
    }
  }

  /**
   * Submission of contact form.
   * Replaces $this->postContact as of v. 2.0.9.
   *
   * @param array $data
   *   Data to create/update a contact.
   *   Requires a 'email_address' key.
   *   But can also accept 'first_name' and 'last_name' key.
   * @param array $listIDs
   *   An array of list UUIDs to post the contact to.
   *
   * @return array
   *   Returns an error if there is an error.
   *   Otherwise it sends the info to other methods.
   *
   * @see https://v3.developer.emailoctopus.com/api_guide/contacts_create_or_update.html
   */
  public function submitContactForm(array $data = [], $listIDs = []) {
    $config = $this->getConfig();

    $enabled = $this->config->get('emailoctopus.enabled_lists')->getRawData();

    if (!$config['api_key'] || !$config['api_key'] || !$data) {
      $msg = 'Missing credentials for postContact';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    if($listIDs != '' && !is_array($listIDs)) {
      $listIDs = [$listIDs];
    }

    if (!$listIDs || count($listIDs) === 0) {
      $msg = 'A listID is required.';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    foreach ($listIDs as $listID) {
      if (!isset($enabled[$listID]) || $enabled[$listID] !== 1) {
        $url = Url::fromRoute('emailoctopus.lists', [], ['absolute' => TRUE]);

       // get the path as string (not recommended)
        $list_enable_path = $url->toString();

        $msg = 'The listID provided does not exist or is not enabled. Please check the emailoctopus available lists from <a href="'.$list_enable_path.'">here</a>';

        $this->loggerFactory->error($msg);

        return [
          'error' => $msg,
        ];
      }
    }

    if (!isset($data['email_address'])) {
      $msg = 'An email address is required';

      $this->loggerFactory->error($msg);

      return [
        'error' => $msg,
      ];
    }

    // Refresh our tokens before every request.
    $this->refreshToken();

    $body = (object)[];

    $body = $this->buildResponseBody($data, $body);

    // Add required fields.
    $body->email_address = $data['email_address'];
    $body->list_memberships = $listIDs;

    $this->moduleHandler->invokeAll('emailoctopus_contact_data_alter', [$data, &$body]);
    $this->moduleHandler->invokeAll('emailoctopus_contact_form_submission_alter', [$data, &$body]);

    try {

      $api_key = $config['api_key'];
      if(isset($config['api_key']) && $config['api_key']) {

        $convertkitAPI = new EmailOctopusPlugin($api_key);
        foreach ($listIDs as $listID) {
          $list = $convertkitAPI->getMailingList($listID);
          if($list) {

            $email_address = $data['email_address']??'';
            if($email_address) {
              $custom_fields = $data['custom_fields']??[];

              $contact = new Contact();
              $contact
              ->setEmail($email_address);

              if(isset($custom_fields['FirstName'])) {
               $contact->setFirstName($custom_fields['FirstName']);
             }
             if(isset($custom_fields['LastName'])) {
               $contact->setLastName($custom_fields['LastName']);
             }

             $response = $convertkitAPI->createContact($contact, $list);

             $this->handleResponse($response, 'submitContactForm');
           }
         }
       }
     }

   }
   catch (RequestException $e) {
      // Return the error to show an error on form submission
    return $this->handleRequestException($e);
  }
  catch (ClientException $e) {
    $this->handleRequestException($e);
  }
  catch (\Exception $e) {

    $this->loggerFactory->error($e->getMessage() ." Submission report for the email: ". $data['email_address']. '');

      // Return the error to show an error on form submission
    return ['error' => $e];
  }
}

  /**
   *  Unsubscribes a contact from all lists.
   *
   * @param array $contact
   *   The response from $this->getDeleted.
   * @param array $data
   *   The $data provided
   *
   * @see https://v3.developer.emailoctopus.com/api_reference/index.html#!/Contacts/putContact
   *
   */
  public function unsubscribeContact(array $data, $listIDs = []) {}

  /**
   * Updates a contact if it already exists on a list.
   *
   * @param array $data
   *   Provided data to update the contact with.
   * @param object $contact
   *   Contact match to provided data.
   * @param array $listIDs
   *   An array of list UUIDs that the contact is being updated on.
   *
   * @see https://v3.developer.emailoctopus.com/api_reference/index.html#!/Contacts/putContact
   */
  private function updateContact(array $data, $contact, $listIDs) {
    $config = $this->getConfig();

    if ($contact && property_exists($contact, 'contact_id')) {

      $body = $contact;
      $body = $this->buildResponseBody($data, $body);

      // Add required fields
      $body->email_address->address = $data['email_address'];
      $body->email_address->permission_to_send = 'implicit';
      $body->update_source = 'Contact';
      $body->list_memberships = $listIDs;

      $this->moduleHandler->invokeAll('emailoctopus_contact_data_alter', [$data, &$body]);
      $this->moduleHandler->invokeAll('emailoctopus_contact_update_data_alter', [$data, &$body]);

      try {
        $response = $this->httpClient->request('PUT', $config['contact_url'] . '/' . $contact->contact_id, [
          'headers' => [
            'Authorization' => 'Bearer ' . $config['api_key'],
            'cache-control' => 'no-cache',
            'content-type' => 'application/json',
            'accept' => 'application/json',
          ],
          'body' => json_encode($body),
        ]);

        return $this->handleResponse($response, 'updateContact');

      }
      catch (RequestException $e) {
        // Return the error to show an error on form submission
        return $this->handleRequestException($e);
      }
      catch (ClientException $e) {
        $this->handleRequestException($e);
      }
      catch (\Exception $e) {
        $this->loggerFactory->error($e->getMessage());

        // Return the error to show an error on form submission
        return ['error' => $e];
      }
    }
    else {
      $this->loggerFactory->error('error: No contact id provided for updateContact method');
      return ['error: No contact id provided'];
    }
  }

  protected function updateTokenExpiration() {
    $tokens = $this->config->getEditable('emailoctopus.tokens');
    $tokens->set('expires', strtotime('now +2 hours'));
    $tokens->save();
  }

}
