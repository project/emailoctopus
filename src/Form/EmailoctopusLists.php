<?php

namespace Drupal\emailoctopus\Form;

use Drupal\emailoctopus\Service\Emailoctopus;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class EmailoctopusLists.
 *
 * Configuration form for enabling lists for use.
 * (ex: in either blocks or REST endpoints.)
 */
class EmailoctopusLists extends ConfigFormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\emailoctopus\Service\Emailoctopus.
   *
   * @var \Drupal\emailoctopus\Service\Emailoctopus
   *   Constant contact service.
   */
  protected $emailOctopus;

  /**
   * EmailoctopusLists constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal\Core\Config\ConfigFactoryInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal\Core\Messenger\MessengerInterface.
   * @param \Drupal\emailoctopus\Service\Emailoctopus $emailOctopus
   *   Constant contact service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, Emailoctopus $emailOctopus) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('emailoctopus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'emailoctopus_lists';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'emailoctopus.enabled_lists',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->emailOctopus->getConfig();
    $enabled = $this->config('emailoctopus.enabled_lists')->getRawData();

    $header = [
      'name' => $this->t('List Name'),
      'list_id' => $this->t('List UUID'),
    ];

    $output = $defaultValue = [];

    if (isset($config['api_key'])) {
      $lists = $this->emailOctopus->getMailingLists();

      if ($lists && count($lists) > 0) {
        foreach ($lists as $list) {
          $output[$list->getId()] = [
            'name' => $list->getName(),
            'list_id' => $list->getId(),
          ];

          $defaultValue[$list->getId()] = isset($enabled[$list->getId()]) && $enabled[$list->getId()] === 1 ? $list->getId() : NULL;
        }
      }
    }

    $form['lists'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $output,
      '#default_value' => $defaultValue,
      '#empty' => $this->t('You must authorize Emailoctopus before enabling a list or there are no lists available on your account.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('emailoctopus.enabled_lists');
    $config->clear('emailoctopus.enabled_lists');
    $enabled = $form_state->getValues()['lists'];
    $lists = $this->emailOctopus->getMailingLists();

    foreach ($enabled as $key => $value) {
      $config->set($key, ($value === 0 ? 0 : 1));
    }

    $config->save();

    $this->emailOctopus->saveContactLists($lists);

    $this->messenger->addMessage($this->t('Your configuration has been saved'));
  }

}
