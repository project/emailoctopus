<?php

namespace Drupal\emailoctopus\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;
use Drupal\emailoctopus\Service\Emailoctopus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class EmailoctopusConfig.
 *
 * Configuration form for adjusting content for the social feeds block.
 */
class EmailoctopusConfig extends ConfigFormBase {

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Symfony\Component\HttpFoundation\RequestStack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Drupal\emailoctopus\Service\Emailoctopus.
   *
   * @var \Drupal\emailoctopus\Service\Emailoctopus
   *   Constant contact service.
   */
  protected $emailOctopus;

  /**
   * EmailoctopusConfig constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Drupal\Core\Config\ConfigFactoryInterface.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal\Core\Messenger\MessengerInterface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Symfony\Component\HttpFoundation\RequestStack.
   * @param \Drupal\emailoctopus\Service\Emailoctopus $emailOctopus
   *   Constant contact service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, MessengerInterface $messenger, RequestStack $request_stack, Emailoctopus $emailOctopus) {
    parent::__construct($config_factory);
    $this->messenger = $messenger;
    $this->requestStack = $request_stack;
    $this->emailOctopus = $emailOctopus;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('messenger'),
      $container->get('request_stack'),
      $container->get('emailoctopus')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'emailoctopus_configure';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'emailoctopus.config',
      'emailoctopus.pkce',
      'emailoctopus.tokens',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $settings = $this->emailOctopus->getConfig();

    $ApiKey = isset($settings['api_key']) ? $settings['api_key'] : NULL;
    $configType = isset($settings['config_type']) ? $settings['config_type'] : 'config';
    $accessToken = isset($settings['api_key']) ? $settings['api_key'] : NULL;
    $codeChallenge = null;

    $form['auth'] = [
      '#type' => 'details',
      '#title' => $this->t('Authorization Settings'),
      '#collapsible' => TRUE,
      '#open' => (!$ApiKey),
    ];

    $form['auth']['message'] = [
      '#markup' => $configType === 'settings.php' ? '<p>' . $this->t('<strong>NOTE:</strong> Application settings were found in your <strong>settings.php</strong> file. Please update information there or remove to use this form.') . '</p>' : '<p>' . $this->t('<strong>NOTE:</strong> Application settings are more secure when saved in your <strong>settings.php</strong> file. Please consider moving this information there. Example:') . '</p><pre>  $settings[\'emailoctopus\'] = [
      \'api_key\' => \'your_api_key\',
    ];</pre>',
  ];

  $form['auth']['api_key'] = [
    '#type' => 'textfield',
    '#title' => $this->t('API Key'),
    '#default_value' => $ApiKey ? $ApiKey : NULL,
    '#required' => TRUE,
    '#disabled' => $configType === 'settings.php' ? TRUE : FALSE
  ];

  $form = parent::buildForm($form, $form_state);

  if ($configType === 'settings.php') {
    unset($form['actions']['submit']);
  }
  
  return $form;
}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('emailoctopus.config');
    $ApiKey = $form_state->getValue('api_key');

    $config->clear('emailoctopus.config');
    $config->set('api_key', $ApiKey);
    $config->save();

    $this->messenger->addMessage($this->t('Your configuration has been saved'));
  }

}
