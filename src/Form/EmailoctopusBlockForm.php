<?php

namespace Drupal\emailoctopus\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\emailoctopus\Service\Emailoctopus;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Class EmailoctopusBlockForm.
 *
 * Creates a form for block on frontend to post
 * contact info and send to Emailoctopus.
 */
class EmailoctopusBlockForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  protected $formIdentifier;

  /**
   * Drupal\Core\Messenger\MessengerInterface.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   *   Messenger Interface.
   */
  protected $messenger;

  /**
   * Drupal\emailoctopus\Service\Emailoctopus.
   *
   * @var \Drupal\emailoctopus\Service\Emailoctopus
   *   Constant contact service.
   */
  protected $emailOctopus;

  /**
   * \Drupal\Core\Extension\ModuleHandlerInterface
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   *   Module handler interface
   */
  protected $moduleHandler;

  /**
   * EmailoctopusBlockForm constructor.
   *
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   MessengerInterface.
   * @param \Drupal\emailoctopus\Service\Emailoctopus $emailOctopus
   *   Constant contact service.
   */
  public function __construct(MessengerInterface $messenger, Emailoctopus $emailOctopus, ModuleHandlerInterface $moduleHandler) {
    $this->messenger = $messenger;
    $this->emailOctopus = $emailOctopus;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('messenger'),
      $container->get('emailoctopus'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setFormIdentifier($formIdentifier) {
    $this->formIdentifier = $formIdentifier;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    $form_id = 'emailoctopus_sigup_form';
    if ($this->formIdentifier) {
      $form_id .= '-' . $this->formIdentifier;
    }

    return $form_id;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $listConfig = []) {
    // Don't show anything if we don't have a list_id set.
    if (!isset($listConfig['list_id'])) {
      return NULL;
    }

    if (isset($listConfig['success_message']) && $listConfig['success_message']) {
      $form_state->set('success_message', $listConfig['success_message']);
    }

    if (isset($listConfig['body']) && isset($listConfig['body']['value'])) {
      $form['body'] = [
        '#markup' => $listConfig['body']['value'],
      ];
    }

    // Custom Fields
    if (isset($listConfig['custom_fields']) && count($listConfig['custom_fields']) > 0) {
      foreach ($listConfig['custom_fields'] as $id => $values) {
        if ($values['display'] === 1) {

          $values['type'] =  strtolower($values['type']);
          if ($values['type'] === 'date' && $this->moduleHandler->moduleExists('datetime')) {
            $form['custom_field__' . $id] = [
              '#type' => 'date',
              '#title' => $this->t($values['label']),
              '#required' => $values['required'] === 1,
            ];
          } else if ($values['type'] === 'text') {
            $form['custom_field__' . $id] = [
              '#type' => 'textfield',
              '#title' => $this->t($values['label']),
              '#required' => $values['required'] === 1,
            ];
          }
        }
      }
    }

    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Email Address'),
      '#required' => TRUE,
    ];

    // Add our list_id into the form.
    if ($listConfig['list_id'] === 'emailoctopus_multi') {
      $list_ids = array_filter(array_values($listConfig['lists']));
      $options = [];

      // Need at least one list_id
      if (count($list_ids) === 0) {
        return null;
      }

      foreach ($list_ids as $id) {
        $options[$id] = $listConfig['lists_all'][$id]->name;
      }

      if ($listConfig['lists_user_select'] === 1) {
        $form['list_id'] = [
          '#type' =>  'checkboxes',
          '#title' => isset($listConfig['lists_select_label']) ? $listConfig['lists_select_label'] : $this->t('Sign me up for:'),
          '#required' => true,
          '#options' => $options
        ];
      } else {
        $form['list_id'] = [
          '#type' => 'value',
          '#value' => $list_ids,
        ];
      }

      
    } else {
      $form['list_id'] = [
        '#type' => 'value',
        '#value' => $listConfig['list_id'],
      ];
    }

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Sign Up'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $message_type = 'status';

    $data = [
      'email_address' => $values['email'],
    ];

    // Add custom field values.
    // Skip adding it if there's no value.
    $fieldKeys = array_keys($values);
    foreach ($fieldKeys as $field) {
      if (strpos($field, 'custom_field__') !== false && isset($values[$field]) && $values[$field]) {
        $data['custom_fields'][str_replace('custom_field__', '', $field)] = $values[$field];
      }
    }

    $lists = [];

    if (is_string($values['list_id'])) {
      $lists = [$values['list_id']];
    } else {
      $lists = array_filter(array_values($values['list_id']));
    }

    $response = $this->emailOctopus->submitContactForm($data, $lists);

    if (isset($response['error'])) {
      $message = 'There was a problem signing you up. Please try again later.';
      $message_type = 'error';
    }
    else {
      if ($form_state->get('success_message')) {
        $message = $form_state->get('success_message');
      }
      else {
        $message = $this->t('You have been signed up. Thank you.');
      }
    }

    $this->messenger->addMessage(strip_tags($message), $message_type);
  }

}
